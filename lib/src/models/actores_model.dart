class Actores {
  List<Actor> items = new List();

  Actores();

  Actores.fromJsonList(List<dynamic> jsonList) { 
    if( jsonList == null ) { return; }


    for( var item in jsonList ) { 

      final pelicula = new Actor.fromJsonMap( item );
      items.add( pelicula );
      
    }
  }
}

class Actor {
  int castId;
  String character;
  String creditId;
  int gender;
  int id;
  String name;
  int order;
  String profilePath;

  Actor({
    this.castId,
    this.character,
    this.creditId,
    this.gender,
    this.id,
    this.name,
    this.order,
    this.profilePath,
  });

  Actor.fromJsonMap(Map<String,dynamic> json) {

    castId            = json['cast_id'];         
    character         = json['character'];
    creditId          = json['credit_id'];
    gender            = json['gender'];
    id                = json['id'];
    name              = json['name'];
    order             = json['order'];
    profilePath       = json['profile_path'];

  }

  getFoto() {
    
    if( profilePath == null){
      return '';
      //return 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/No_image_3x4.svg/1280px-No_image_3x4.svg.png';
    }

    return 'https://image.tmdb.org/t/p/w500/$profilePath';
  }

}